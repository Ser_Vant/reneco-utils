'use strict';

module.exports = function encodeParams (params) {
    return Object.keys(params).map(key => {
        let value = params[key];
        return (value !== undefined && value !== null)
            && (encodeURIComponent(key) + '=' + encodeURIComponent(value));
    });
};
