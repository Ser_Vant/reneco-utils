'use strict';
const isObject = require('./isObject');

module.exports = function getObjectCopy(o) {
    if(Array.isArray(o)) {
        return o.map(o1 => getObjectCopy(o1));
    } else if(isObject(o)) {
        return Object.keys(o).reduce((list, k) => {
            list[k] = getObjectCopy(o[k]);
            return list;
        }, {})
    } else {
        return o;
    }

}