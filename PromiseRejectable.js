'use strict'
module.exports = class RejectablePromise {
    constructor(callback){
        if(callback) {
            this._promise = (new Promise((resolve, reject) => {
                    this._resolve = resolve;
                    this._reject = reject;
                    callback(resolve, reject);
                }));
        }
    }
    static resolve(value) {
        return (promise => {
            promise._promise = Promise.resolve(value);
            return promise;
        })(new RejectablePromise());
    }
    static reject(value) {
        return (promise => {
            promise._promise = Promise.reject(value);
            return promise;
        })(new RejectablePromise());
    }
    resolve(value) {
        this._resolve && this._resolve(value);
        return this;
    }
    reject(value) {
        this._reject && this._reject(value);
        return this;
    }
    _getChildPromise(act, callback) {
        return (promise => {
            promise._promise = this._promise[act](callback);
            return promise;
        })(new RejectablePromise());
    }
    then(callback) {
        return this._getChildPromise('then', callback);
    }
    catch(callback) {
        return this._getChildPromise('catch', callback);
    }
    finally(callback) {
        return this._getChildPromise('finally', callback);
    }
}
// const withEvents =  require('./withEvents'),
//     EVENTS = ['resolve', 'reject', 'finally'].reduce((list, v) => {
//         list[v] = v;
//         return list;
//     }, {});
// module.exports = withEvents(class RejectablePromise {
//     static EVENTS = EVENTS;
//     constructor(callback) {
//         this._promise = this.__createPromise(callback);
//     }
//     __createPromise(callback) {
//         return new Promise((resolve, reject) => {
//             // console.log(callback)
//             this._resolve = resolve;
//             this._reject = reject;
//             callback(resolve, reject);
//         })
//         .finally(result => {
//             this._resolve = null;
//             this._reject = null;
//             return result;
//         });
//     }
//     then(callback) {
//         this._promise = this._promise.then(result => callback && callback(result) || result);
//         return this;
//     }
//     catch(callback) {
//         this._promise = this._promise.catch(callback);
//         return this;
//     }
//     finally(callback) {
//         this._promise = this._promise.finally(callback);
//         return this;
//     }
//     resolve(result) {
//         if(this._resolve) {
//             this.dispatchEvent(EVENTS.resolve, result);
//             this.dispatchEvent(EVENTS.finally, result);
//             this._resolve(result);
//         }
//     }
//     reject(result) {
//         if(this._reject) {
//             this.dispatchEvent(EVENTS.reject, result);
//             this.dispatchEvent(EVENTS.finally, result);
//             this._reject(result);
//         }
//     }
// });