'use strict';
const firstLetterUpper = require('./firstLetterUpper');

module.exports = function fromCSS(style) {
    return style
        .split('-')
        .map((part, ix) => ix > 0 ? firstLetterUpper(part) : part)
        .join('');
};
