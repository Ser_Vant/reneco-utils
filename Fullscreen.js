import isFoo from './isFoo';

export default class Fullscreen {
    static isEnabled() {
        return !!(document.webkitFullscreenEnabled
            || document.fullscreenEnabled
            || document.mozFullScreenEnabled);
    }
    static isFullscreen() {
        return !!(document.webkitFullscreenElement
            || document.fullscreenElement
            || document.mozFullScreenElement)
    }
    constructor(options) {
        if (options && options.onChange) {
            this.onChange(
                options.element || document.body,
                options.onChange
            )
        }
        this.isShow = options && options.fullscreen === true;
        if(this.isShow) {
            this.fullscreen()
        }
    }
    onChangeState(e) {
        this.callback(Fullscreen.isFullscreen());
    }
    onChange(element, callback) {
        if(element && isFoo(element)) {
            callback = element;
            element = null;
        }
        if(!element) {
            element = document.body;
        }
        if (isFoo(callback)) {
            this.element = element;
            this.callback = callback;
            element.addEventListener('webkitfullscreenchange', this.onChangeState.bind(this));
            element.addEventListener('mozfullscreenchange', this.onChangeState.bind(this));
            element.addEventListener('fullscreenchange', this.onChangeState.bind(this));
        }
    }
    fullscreen() {
        let element = this.element;
        if(element) {
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            }
        } else {
            console.error('no element specified')
        }
    }
    exitFullscreen() {
        if(this.element) {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.cancelFullscreen) {
                document.cancelFullscreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            } else if (document.mozCancelFullscreen) {
                document.mozCancelFullscreen();
            }
        }
    }
    destroy() {
        if(this.element && this.callback) {
            this.element.removeEventListener('webkitfullscreenchange', this.onChangeState.bind(this));
            this.element.removeEventListener('mozfullscreenchange', this.onChangeState.bind(this));
            this.element.removeEventListener('fullscreenchange', this.onChangeState.bind(this));
        }
        this.element = null;
        this.callback = null;
    }
}
