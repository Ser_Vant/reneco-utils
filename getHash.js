'use strict';
module.exports = function getHash(data) {
    return Object.keys(data || {})
        .sort()
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`)
        .join('&');
}