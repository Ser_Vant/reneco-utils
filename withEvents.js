const getPromised = require('./getPromised'),
    isFoo = require('./isFoo');
module.exports = function withEvents(oClass) {
    if(oClass.prototype.addEventListener === undefined) {
        oClass.prototype.addEventListener = function(eName, callback) {
            if(!eName) {
                throw new Error('No event specified!')
            }
            this._events = this._events || {};
            this._events[eName] = this._events[eName] || [];
            const ix = this._events[eName].indexOf(callback);
            if(ix < 0 && isFoo(callback)) {
                this._events[eName].push(callback);
            }
        };
        oClass.prototype.removeEventListener = function(eName, callback) {
            if(this._events && this._events[eName]) {
                if(!callback) {
                    delete this._events[eName];
                } else {
                    const ix = this._events[eName].indexOf(callback);
                    if(ix > -1) {
                        this._events[eName].splice(ix, 1);
                    }
                }
            }
        };
        oClass.prototype.dispatchEvent = function(eName, params) {
            if(this._events && this._events[eName]) {
                if(this.shouldDispatch) {
                    getPromised(this.shouldDispatch())
                        .then(should => should && this._events[eName].forEach(callback => callback(params)))
                        .catch(() => false);
                } else {
                    this._events[eName].forEach(callback => callback(params));
                }
            }
        };
        oClass.prototype.destroyEvents = function() {
            if(this._events) {
                this._events = null;
            }
        }
    }
    return oClass;
}