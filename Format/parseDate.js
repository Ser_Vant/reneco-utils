module.exports = unformatted => (
    unformatted instanceof Date
        ? {
            year: String(unformatted.getFullYear()).padStart(4, '0'),
            month: String(unformatted.getMonth()+1).padStart(2, '0'),
            day: String(unformatted.getDate()).padStart(2, '0'),
            hours: String(unformatted.getHours()).padStart(2, '0'),
            minutes: String(unformatted.getMinutes()).padStart(2, '0'),
            seconds: String(unformatted.getSeconds()).padStart(2, '0'),
            milliseconds: String(unformatted.getMilliseconds()).padStart(3, '0')
        }
        : (match => ({
            year: match[1],
            month: match[2],
            day: match[3],
            hours: match[4],
            minutes: match[5],
            seconds: match[6],
            milliseconds: match[7]
        }))(/^(\d{4})-(\d{2})-(\d{2})\D+(\d{2}):(\d{2}):(\d{2})\.(\d+)$/.exec(unformatted))
);