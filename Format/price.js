const formatFloat = require('./float'),
    markTriades = require('./markTriades'),
    FormatPrice = ({price, precision}) => {
    const splitted = String(formatFloat(price)).replace(/[.,]0+$/, '').split(/[.,]/);
        return markTriades(splitted[0])
            + (
                splitted[1]
                    ? `.${markTriades(splitted[1])}`
                    : ''
            );
    };

FormatPrice.defaultProps = {
    price: 0,
    precision: 8
}

module.exports = FormatPrice;