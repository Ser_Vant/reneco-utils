module.exports = price => price
    && String(price).toUpperCase().indexOf('E') >-1
    && parseFloat(price).toFixed(20).replace(/0+$/, '')
    || price
    || 0;
