const parseDate = require('./parseDate');

module.exports = ({unformatted, year, month, day, hours, minutes, seconds, milliseconds}) => {
    if(unformatted) {
        ({year, month, day, hours, minutes, seconds, milliseconds} = parseDate(unformatted));
    }
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}.${milliseconds}`;
};