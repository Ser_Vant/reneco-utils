'use strict';
const getArrayItemCircular = require('./getArrayItemCircular'),
    toRoman = require('./toRoman');

module.exports = (depth, counter) => getArrayItemCircular([
    c => c,
    c => getArrayItemCircular(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'], c-1),
    c => toRoman(c)
], depth)(counter);