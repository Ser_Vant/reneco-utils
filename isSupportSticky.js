'use strict';
const uCache = require('./uCache');

module.exports = function isSupportSticky() {
    const STICKY = 'sticky';
    if (uCache[STICKY] === undefined) {
        uCache[STICKY] = (d => {
            let finded = [STICKY, `-webkit-${STICKY}`, `-moz-${STICKY}`, `-o-${STICKY}`, `-ms-${STICKY}`].findIndex(prop => {
                try {
                    d.style.position = prop;
                } catch (e) {
                    return false;
                }
                return d.style.position === prop;
            });
            return finded > -1;
        })(document.createElement('div'));
    }
    return uCache[STICKY];
}
