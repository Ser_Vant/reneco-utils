'use strict';
const getDocumentScroll = require('./getDocumentScroll');

module.exports = function getOffsetRect (elem) {
    let box = elem.getBoundingClientRect(),
        body = document.body,
        docElem = document.documentElement,
        scroll = getDocumentScroll(),
        clientTop = docElem.clientTop || body.clientTop || 0,
        clientLeft = docElem.clientLeft || body.clientLeft || 0,
        top = box.top + scroll.y - clientTop,
        left = box.left + scroll.x - clientLeft;
    return {
        y: Math.round(top),
        x: Math.round(left)
    };
}
