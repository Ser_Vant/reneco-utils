'use strict';

const parseGetString = require('./parseGetString');

module.exports = function parseUrl (url) {
    let ret = {},
        re = /((https?):\/)?(\/([^/]+))?([^?#]+)?(\?([^#]+))?(#(.+))?/,
        parsed = re.exec(url);

    if (parsed && parsed.length > 0) {
        ret.protocol = parsed[2];
        ret.host = parsed[4];
        ret.pathname = parsed[5];
        ret.hash = parsed[9];
        ret.query = parsed[7] && parseGetString(parsed[7]);
    }
    return ret;
};
