'use strict';
module.exports = (list, cur) => list[cur < list.length
    ? cur
    : (cur - Math.floor(cur/list.length)*list.length)];