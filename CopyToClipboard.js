'use strict';
module.exports = function CopyToClipboard(text) {
    return new Promise((resolve, reject) => {
        let textArea = document.createElement('textarea'),
            answer;
        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.select();
        try {
            answer = document.execCommand('copy');
        } catch (err) {
            console.error('Oops, unable to copy');
        }
        document.body.removeChild(textArea);
        if(answer) {
            resolve();
        } else {
            reject();
        }
    });

}
