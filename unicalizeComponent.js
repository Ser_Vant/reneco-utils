'use strict';

let counter = 0;

module.exports = function unicalizeComponent(components) {
    if(!Array.isArray(components)) {
        components = [components];
    }
    components.forEach(component => {
        if(component.uuid === undefined) {
            component.uuid = counter++;
            component.isInstance = inst => {
                return inst && inst.type && inst.type.uuid === component.uuid;
            }
        }
    });
}