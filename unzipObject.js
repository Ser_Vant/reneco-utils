'use strict';

const getChunkName = (name, parents) => parents
    ? (parent => `${parent[0]}${(parent.slice(1).concat([name])).map(item => {
        return '[' + item + ']';
    }).join('')}`)(parents.slice())
    : name;

module.exports = function unzipObject(data, parent){
    return Object.keys(data || {}).map(name => {
        const value = data[name];
        if(value !== undefined && value !== null) {
            if (Array.isArray(value)) {
                return encodeURIComponent(getChunkName(name, parent)) + '=' + value.map(item => encodeURIComponent(item)).join(',');
            } else if (value instanceof Object) {
                return unzipObject(value, (parent || []).concat([name]));
            } else if (parent) {
                return encodeURIComponent(getChunkName(name, parent)) + '=' + encodeURIComponent(value);
            } else {
                return encodeURIComponent(name) + '=' + encodeURIComponent(value);
            }
        }
    }).reduce((list, item) => item && list.concat([item]) || list, []).join('&');
}
