'use strict';

module.exports = function firstLetterUpper (str) {
    return str && str[0].toUpperCase() + ((s => {
        return s.slice(1);
    })(str.split(''))).join('') || '';
}
