module.exports = function isArrayEqual(ar1, ar2, compareFunction) {
    compareFunction = compareFunction || ((a, b) => a === b);
    if(!Array.isArray(ar1) || !Array.isArray(ar2) || ar1.length !== ar2.length) {
        return false;
    } else {
        return ar1.findIndex((i1, ix) => !compareFunction(i1, ar2[ix])) < 0;
    }
}