'use strict';

const withEvents = require('../withEvents');

module.exports = function renodable(oClass) {
    let counter = 0;
    if(!oClass.__IS_RENODE__) {
        const rClass = function() {
            oClass.constructor && oClass.constructor.apply(rClass, arguments);
            console.log(this);
        }
        Object.keys(oClass).forEach(k => {
            rClass[k] = oClass[k];
        })

        Object.setPrototypeOf(rClass.prototype, oClass.prototype);
        console.log(rClass, oClass);

        return rClass;
    }

    return oClass;
}