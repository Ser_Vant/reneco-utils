'use strict';

const withEvents = require('../withEvents'),
    LIST_EVENTS = {
        add: 'LIST_ADD',
        remove: 'LIST_REMOVE',
        reorder: 'LIST_REORDER',
        change: 'LIST_CHANGE'
    };

module.exports = withEvents(class ReNodeList {
    _list = [];
    static EVENTS = LIST_EVENTS;
    push(node, position, silent) {
        if(position < 0) {
            this._list.push(node);
        } else {
            this._list.splice(position, 0, node);
        }
        !silent && this.dispatchEvent(LIST_EVENTS.add, node);
        !silent && this.dispatchEvent(LIST_EVENTS.change, this);
    }
    find(callback) {
        return this._list.find(callback);
    }
    findIndex(callback) {
        return this._list.findIndex(callback);
    }
    forEach(callback) {
        return this._list.forEach(callback);
    }
    map(callback) {
        return this._list.map(callback);
    }
    remove(callback, silent) {
        const ix = this._list.findIndex(callback);
        if(ix > -1) {
            const node = this._list[ix];
            this._list.splice(ix, 1);
            !silent && this.dispatchEvent(LIST_EVENTS.remove, node);
            !silent && this.dispatchEvent(LIST_EVENTS.change, this);
        }
    }
    clear(silent) {
        !silent && this._list.forEach(node => this.dispatchEvent(LIST_EVENTS.remove, node));
        this.list = [];
        !silent && this.dispatchEvent(LIST_EVENTS.change, this);
    }
    filter(callback) {
        return this._list.filter(callback);
    }
    destruct() {
        this._list = null;
    }
    reorder(orderList, silent) {
        if(orderList.length === this.length && orderList.indexOf(-1)<0) {
            this._list = orderList.map(k => this._list[k]);
            !silent && this.dispatchEvent(LIST_EVENTS.change, this);
        }
    }
    get length() {
        return this._list && this._list.length || 0;
    }
});

module.EVENTS = LIST_EVENTS;