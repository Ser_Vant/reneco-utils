'use strict';

const withEvents = require('../withEvents'),
    ReNodeList = require('./ReNodeList'),
    RENODE_EVENTS = {
        change: 'RENODE_CHANGE'
    };

let counter = 0;

module.exports = withEvents(class ReNode {
    _ix = counter++;
    __IS_RENODE__ = true;
    _childs = new ReNodeList();
    _relations = new ReNodeList();
    _reverseRelations = new ReNodeList();
    _parent = null;
    static EVENTS = RENODE_EVENTS;
    static isInstance(node) {
        return node.__IS_RENODE__ === true;
    }
    constructor(props = {}) {
        (props.childs || []).forEach(child => this.addChild(child, true));
        (props.relations || []).forEach(relation => this.addRelation(relation, true));
        this._childs.addEventListener(ReNodeList.EVENTS.change, () => this.dispatchEvent(RENODE_EVENTS.change));
    }
    getIX() { return this._ix }
    canAdd2List(list, object) {
        return ReNode.isInstance(object) && !list.find(item => item.getIX() === object.getIX());
    }
    addChild(ch, position, silent) {
        if(this.canAdd2List(this._childs, ch)) {
            this._childs.push(ch, position, silent);
            ch.setParent(this);
        }
    }
    removeChild(item, silent) {
        this._childs.remove(i => item.getIX() === i.getIX(), silent);
    }
    reorder(orderlist, silent) {
        this._childs.reorder(orderlist, silent);
    }
    clearChilds(silent) {
        this._childs.clear(silent);
    }
    setParent(parent) {
        this._parent = parent;
    }
    addRelation(relation, silent) {
        if(this.canAdd2List(this._relations, relation)) {
            this._relations.push(relation, silent);
            if(relation.canAdd2List(relation._reverseRelations, this)) {
                relation._reverseRelations.push(this, true);
            }
        }
    }
    findChild(callback) {
        return this._childs.find(callback);
    }
    filterChilds(callback) {
        return this._childs.filter(callback);
    }
    childs() {
        return this._childs;
    }
    get length() {
        return this._childs.length;
    }
    get parent() {
        return this._parent;
    }
    destruct() {
        this._childs.forEach(child => child.destruct());
        this._relations.forEach(relation => relation._reverseRelations.remove(item => item.getIX() === this.getIX()));
        this._reverseRelations.forEach(relation => relation._relations.remove(item => item.getIX() === this.getIX()));
        if(this._parent) {
            this._parent._childs.remove(item => item.getIX() === this.getIX());
        }
        this._childs.destruct();
        this._relations.destruct();
        this._reverseRelations.destruct();
        this._childs = null;
        this._relations = null;
        this._reverseRelations = null;
    }
});