'use strict';
const Pony = require('pony');

module.exports = function getItemIX(item, ix) {
    return item
        && (
            (item instanceof Pony.Instance && item._cid_)
            || (item.props.obj
            && (
                item.props.obj._cid_
                || item.props.obj.id
            ))
            || item.props['data-ix']
            || ix
        )
        || ix;
}
