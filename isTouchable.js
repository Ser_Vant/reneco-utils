'use strict';
module.exports = function isTouchable() {
    return (('ontouchstart' in window) ||
        (window.navigator.maxTouchPoints > 0) ||
        (window.navigator.msMaxTouchPoints > 0));
}
