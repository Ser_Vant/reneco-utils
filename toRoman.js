'use strict';
module.exports = a => (([result]) => result)([
    [1000, '\u217F'],
    [900, '\u217D\u217F'],
    [500, '\u217E'],
    [400, '\u217D\u217E'],
    [100, '\u217D'],
    [90, '\u2179\u217D'],
    [50, '\u217C'],
    [40, '\u2179\u217C'],
    [10, '\u2179'],
    [9, '\u2178'],
    [5, '\u2174'],
    [4, '\u2173'],
    [1, '\u2170']
].reduce(([result, n], [arabic, roman]) => [`${result}${roman.repeat(Math.floor(n / arabic))}`, n%arabic], ['', a]));