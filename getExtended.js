'use strict';
const extend = require('./extend');

Function.prototype.clone = function () {
    let that = this,
        temp = function temporary() { return that.apply(this, arguments); };
    for (let key in this) {
        if (this.hasOwnProperty(key)) {
            temp[key] = this[key];
        }
    }
    return temp;
};

if (typeof Object.create !== 'function') {
    Object.create = function (prototype) {
        function F() { }
        F.prototype = prototype;
        return new F();
    };
}

module.exports = function getExtended(obj, newMethods) {
    let f = obj.clone();
    f.prototype = Object.create(obj.prototype);
    f.prototype.constructor = obj;
    return extend(f, newMethods);
}
