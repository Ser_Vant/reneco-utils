'use strict';

const isFoo = require('./isFoo');

module.exports = function call (foo, parameter, context) {
    if (isFoo(foo)) {
        if (context) {
            return foo.call(context, parameter);
        } else {
            return foo(parameter);
        }
    }
}
