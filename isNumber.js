'use strict';
module.exports = function isNumber(num) {
    let p = parseInt(num);
    return !Number.isNaN(p) && !(p % 1);
}
