'use strict';

module.exports = function isEmail(string){
    return (/^[a-z][-.a-z0-9]{0,}@[a-z]{1,}\.[a-z0-9]{0,9}$/).test(string);
};