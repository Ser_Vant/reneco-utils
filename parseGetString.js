'use strict';

module.exports = function parseGetString(url){
    return url.replace(/^\?/, '')
        .split('&')
        .reduce((prev, cur) => {
            const part = cur.split('='),
                nameSplitted = part[0].split('[');
            if(nameSplitted.length > 1) {
                let o = prev;
                nameSplitted.forEach((namePart, ix) => {
                    namePart = namePart.replace(']', '');
                    o[namePart] = ix === nameSplitted.length -1
                        ? (part[1] === undefined
                            ? true
                            : part[1])
                        : o[namePart] || {};
                    o = o[namePart];
                });
            } else if(part[0]){
                prev[part[0]] = part[1] === undefined
                    ? true
                    : part[1];
            }
            return prev;
        }, {});
};
