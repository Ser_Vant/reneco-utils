'use strict';

module.exports = function getClassesObject(classes) {
    return ((classes || '').split(' ') || []).reduce((prev, cur) => {
        prev[cur] = true;
        return prev;
    }, {});
}
