'use strict';

const ajax = require('./ajax'),
    PromiseRejectable = require('./PromiseRejectable');

module.exports = function ajaxPromised(url, options = {}) {
    var xhr, _to;
    const promise = new PromiseRejectable((resolve, reject) => {
        xhr = ajax({
            url: url,
            sync: options.sync,
            isBinary: options.isBinary,
            method: options.method,
            data: options.data,
            dataType: options.dataType,
            contentType: options.contentType,
            progress: options.progress,
            abort: options.abort,
            headers: options.headers || {},
            success: data => {
                _to && clearTimeout(_to);
                if(options && options.debug) {
                    console.log(xhr.getAllResponseHeaders())
                }
                // console.log(data)
                resolve(data);
            },
            error: error => {
                _to && clearTimeout(_to);
                if(options && options.debug) {
                    console.log(xhr.getAllResponseHeaders())
                }
                reject(error);
            }
        });
        _to = setTimeout(()=> {
            reject(Error('Request timeout'));
        }, 30000);
    });
    promise.catch(() => () => xhr && xhr.abort());
    return promise;
};
