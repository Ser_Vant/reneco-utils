module.exports = list => Promise.all(list.map(promise => promise
        .catch(e => Promise.resolve(e))));