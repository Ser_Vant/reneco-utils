module.exports = function toCSS (style) {
    return String(style).replace(/([A-Z])/g, '-$1').toLowerCase();
}