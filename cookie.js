'use strict';

module.exports = {
    delete(name) {
        document.cookie = name + '=;Max-Age=-1';
    },
    parseCookies(str = document.cookie) {
        let list = {};
        str && str.split(';').forEach(cookie => {
            let parts = cookie.split('=');
            list[parts.shift().trim()] = decodeURI(parts.join('='));
        });
        return list;
    },
    getCookieString(c) {
        let str = [],
            n;
        for (n in c) {
            if (c.hasOwnProperty(n)) {
                str.push(encodeURI(n + '=' + c[n]));
            }
        }
        return str.join(';');
    },
    set(name, value, options) {
        document.cookie = [name + '=' + value]
            .concat(
                ['Path', 'Domain', 'Max-Age', 'Expires', 'SameSite'].map(item => {
                    return options && options[item] && item + '=' + options[item];
                })
                    .concat(['Secure', 'HttpOnly', 'Partitioned'].map(item => {
                        return options && options[item] && item;
                    }))
            ).reduce((prevValues, item) => {
                return item && prevValues.concat([item]) || prevValues;
            }, []).join(';');
    },
    get(name) {
        return parseCookies()[name];
    }
};
