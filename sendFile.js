'use strict';

const ajax = require('./ajax');

module.exports = function sendFile(object) {
    let ajaxData = {
        url: object.url,
        file: object.file,
        progress: object.onProgress || object.progress,
        load: object.onLoad || object.load,
        success: object.onSuccess || object.success,
        error: object.onError || object.error,
        messager: object.messager
    };
    return ajax(ajaxData);
}
