'use strict';
const crypto = require('crypto');

module.exports = function getUID() {
    let current_date = (new Date()).valueOf().toString(),
        random = Math.random().toString();
    return crypto.createHash('sha1').update(current_date + random).digest('hex');
};
