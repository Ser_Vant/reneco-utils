module.exports = function defaults(defaultData, data) {
    return Object.assign({}, defaultData, data);
}