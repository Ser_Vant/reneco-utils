'use strict';
module.exports = function getDocumentScroll() {
    let doc = document.documentElement,
        left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0),
        top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    return {
        x: left,
        y: top
    };
}
