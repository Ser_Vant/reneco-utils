'use strict';

const getDocumentScroll = require('./getDocumentScroll'),
    isObject = require('./isObject'),
    scrollPolyfill = require('smoothscroll-polyfill');

scrollPolyfill.polyfill();

module.exports = function scrollTo(position, callback, element){
    let speed = 300,
        tickCnt = 0,
        startY = element && element.scrollTop || getDocumentScroll().y;
    if(isObject(position)) {
        let options = position;
        position = options.position;
        callback = options.callback;
        element = options.element;
        speed = options.speed === undefined && speed || options.speed;
    }
    element = element || window;
    // console.log(startY);
    if (callback) {
        var clb = function () {
            callback();
            element.addEventListener('scroll', clb);
        };
        element.addEventListener('scroll', clb);

    }
    element.scroll({
        top: position,
        left: 0,
        behavior: 'smooth'
    });

    // element.scrollTo(0, position);
    callback && callback();
}
