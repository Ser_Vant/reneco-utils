'use strict';

module.exports = function getObjectValue(obj, key, delimiter = '.') {
    return obj
        && (ix => ix > -1
            ? getObjectValue(obj[key.slice(0, ix)], key.slice(ix+delimiter.length), delimiter)
            : obj[key]
        )((key || '').indexOf(delimiter))
        || undefined;
}