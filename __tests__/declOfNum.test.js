const declOfNum = require('../declOfNum'),
    titles = ['единица', 'единицы', 'единиц'];

test('declOfNum 0', () => {
    const result = declOfNum(0, titles);
    expect(result).toEqual(titles[2]);
});

test('declOfNum 1', () => {
    const result = declOfNum(1, titles);
    expect(result).toEqual(titles[0]);
});

test('declOfNum 2', () => {
    const result = declOfNum(2, titles);
    expect(result).toEqual(titles[1]);
});

test('declOfNum 3', () => {
    const result = declOfNum(3, titles);
    expect(result).toEqual(titles[1]);
});

test('declOfNum 4', () => {
    const result = declOfNum(4, titles);
    expect(result).toEqual(titles[1]);
});

test('declOfNum 5', () => {
    const result = declOfNum(5, titles);
    expect(result).toEqual(titles[2]);
});

test('declOfNum 6', () => {
    const result = declOfNum(6, titles);
    expect(result).toEqual(titles[2]);
});

test('declOfNum 7', () => {
    const result = declOfNum(7, titles);
    expect(result).toEqual(titles[2]);
});

test('declOfNum 8', () => {
    const result = declOfNum(8, titles);
    expect(result).toEqual(titles[2]);
});

test('declOfNum 9', () => {
    const result = declOfNum(9, titles);
    expect(result).toEqual(titles[2]);
});

test('declOfNum 10', () => {
    const result = declOfNum(10, titles);
    expect(result).toEqual(titles[2]);
});

test('declOfNum 827346879211', () => {
    const result = declOfNum(827346879211, titles);
    expect(result).toEqual(titles[2]);
});