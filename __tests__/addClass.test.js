const addClass = require('../addClass');

test('add class `foo` to `bar`', () => {
    const mock1 = 'foo',
        mock2 = 'bar';
        result = addClass(mock1, mock2);
    expect(result).toBe(`${mock1} ${mock2}`);
});

test('add class `foo and baz` to `bar`', () => {
    const mock1 = 'foo',
        mock2 = 'bar',
        mock3 = 'baz';
        result = addClass(mock1, mock2, mock3);
    expect(result).toBe(`${mock1} ${mock2} ${mock3}`);
});

test('add class `foo and baz a1 a2 a4` to `bar`', () => {
    const mock1 = 'foo',
        mock2 = 'bar',
        mock3 = 'baz',
        mock4 = 'a1',
        mock5 = 'a2',
        mock6 = 'a4';
        result = addClass(mock1, mock2, mock3, mock4, mock5, mock6);
    expect(result).toBe(`${mock1} ${mock2} ${mock3} ${mock4} ${mock5} ${mock6}`);
});