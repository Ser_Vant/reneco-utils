const PromiseRejectable = require('../PromiseRejectable');

test('normal work', () => {
    const mock = 'XXX',
        pr = new PromiseRejectable(resolve => setTimeout(() => resolve(mock)));
    return pr.then(result => expect(result).toBe(mock));
});

test('chain work', () => {
    const mock = 'xxxx',
        pr = new PromiseRejectable(resolve => setTimeout(() => resolve(mock)));
    return expect(pr.then(() => {}) instanceof PromiseRejectable).toBeTruthy()
});

test('resolve work', () => {
    let to;
    const mock = 'xxx',
        pr = new PromiseRejectable(resolve => {to = setTimeout(resolve, 10000)});
    return  (new Promise(resolve => {
        pr.then(resolve);
        setTimeout(() => pr.resolve(mock));
    }))
        .then(result => expect(result).toBe(mock))
        .finally(() => { to && clearTimeout(to); })
});

test('reject work', () => {
    let to;
    const mock = 'xxx',
        pr = new PromiseRejectable(resolve => { to = setTimeout(resolve, 10000); });
    return  (new Promise(resolve => {
        pr.catch(resolve);
        setTimeout(() => pr.reject(mock));
    }))
        .then(result => expect(result).toBe(mock))
        .finally(() => { to && clearTimeout(to); })
});