const getUID = require('../getUID');

test('create pair of UID and compare them', () => {
    const result = getUID(),
        result1 = getUID();
    expect(result).toBeDefined();
    expect(result1).toBeDefined();
    expect(result === result1).toBeFalsy();
});