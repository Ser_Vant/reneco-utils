const fromPairs = require('../fromPairs');

test('empty', () => {
    const mock = null,
        result = fromPairs(mock);
    expect(result).toEqual({});
});

test('[[\'a\', 1], [\'b\', 3]]', () => {
    const mock = [['a', 1], ['b', 3]],
        result = fromPairs(mock);
    expect(result).toEqual({
        a: 1,
        b: 3
    });
});

test('[[\'a\', 1], [\'b\', 3]], {f: 3}', () => {
    const mock = [['a', 1], ['b', 3]],
        mock2 = {f: 3};
        result = fromPairs(mock, mock2);
    expect(result).toEqual({
        a: 1,
        b: 3,
        f: 3
    });
});

test('[[\'a\', 1], [\'b\', 3]], {}', () => {
    const mock = [['a', 1], ['b', 3]],
        mock2 = {};
        result = fromPairs(mock, mock2);
    expect(result).toEqual({
        a: 1,
        b: 3
    });
});