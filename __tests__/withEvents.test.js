const withEvents = require('../withEvents'),
    TestClass = withEvents(class Test {
        test() {}
    });

test('event methods is defined', () => {
    const instance = new TestClass();
    expect(instance.addEventListener).toBeDefined();
    expect(instance.removeEventListener).toBeDefined();
    expect(instance.dispatchEvent).toBeDefined();
});

test('functional', () => {
    expect.assertions(4);
    const instance = new TestClass(),
        expectData = {test: 'test'},
        clb = data => expect(data).toEqual(expectData);

    instance.addEventListener('test', clb);
    instance.dispatchEvent('test', expectData);
    instance.removeEventListener('test', clb);
    instance.dispatchEvent('test', expectData);

    expect(instance.addEventListener).toBeDefined();
    expect(instance.removeEventListener).toBeDefined();
    expect(instance.dispatchEvent).toBeDefined();
});

test('multievents', () => {
    expect.assertions(3);
    const instance = new TestClass(),
        expectData = {test: 'test'},
        clb = data => expect(data).toEqual(expectData),
        clb2 = data => expect(data).toEqual(expectData);

    instance.addEventListener('test', clb);
    instance.addEventListener('test', clb2);
    expect(instance._events.test.length).toBe(2);
    instance.removeEventListener('test', clb);
    expect(instance._events.test.length).toBe(1);
    instance.removeEventListener('test', clb);
    expect(instance._events.test.length).toBe(1);
})