const isObjectEqual = require('../isObjectEqual');


test('{}, {}', () => {
    const mock = {},
        mock1 = {},
        result = isObjectEqual(mock, mock1);
    expect(result).toBeTruthy();
});

test('{}, null', () => {
    const mock = {},
        mock1 = null,
        result = isObjectEqual(mock, mock1);
    expect(result).toBeTruthy();
});

test('{}, \'\'', () => {
    const mock = {},
        mock1 = '',
        result = isObjectEqual(mock, mock1);
    expect(result).toBeTruthy();
});

test('{a: 1}, {a: 1}', () => {
    const mock = {a: 1},
        mock1 = {a: 1},
        result = isObjectEqual(mock, mock1);
    expect(result).toBeTruthy();
});

test('{a: 1}, {a: 2}', () => {
    const mock = {a: 1},
        mock1 = {a: 2},
        result = isObjectEqual(mock, mock1);
    expect(result).toBeFalsy();
});

test('{a: []}, {a: 2}', () => {
    const mock = {a: []},
        mock1 = {a: 2},
        result = isObjectEqual(mock, mock1);
    expect(result).toBeFalsy();
});

test('{a: [1,2,3]}, {a: [1,2,3]}', () => {
    const mock = {a: [1,2,3]},
        mock1 = {a: [1,2,3]},
        result = isObjectEqual(mock, mock1);
    expect(result).toBeTruthy();
});

test('{a: [1,2,3]}, {a: [1,2]}', () => {
    const mock = {a: [1,2,3]},
        mock1 = {a: [1,2]},
        result = isObjectEqual(mock, mock1);
    expect(result).toBeFalsy();
});

test('{a: [1,2, {b: {c:1, d: [], e: {}}}]}, {a: [1,2, {b: {c:1, d: [], e: {}}}]}', () => {
    const mock = {a: [1,2, {b: {c:1, d: [], e: {}}}]},
        mock1 = {a: [1,2, {b: {c:1, d: [], e: {}}}]},
        result = isObjectEqual(mock, mock1);
    expect(result).toBeTruthy();
});

test('{a: [1,2, {b: {c:1, d: [0], e: {}}}]}, {a: [1,2, {b: {c:1, d: [], e: {}}}]}', () => {
    const mock = {a: [1,2, {b: {c:1, d: [0], e: {}}}]},
        mock1 = {a: [1,2, {b: {c:1, d: [], e: {}}}]},
        result = isObjectEqual(mock, mock1);
    expect(result).toBeFalsy();
});

test('1, 2', () => {
    const mock = 1,
        mock1 = 2,
        result = isObjectEqual(mock, mock1);
    expect(result).toBeFalsy();
})