const getClasses = require('../getClasses');

test('{a: true}', () => {
    const mock = {a: true},
        result = getClasses(mock);
    expect(result).toBe('a');
});

test('{a: false}', () => {
    const mock = {a: false},
        result = getClasses(mock);
    expect(result).toBe('');
});

test('{a: false, b: true, z: false, y: true}', () => {
    const mock = {a: false, b: true, z: false, y: true},
        result = getClasses(mock);
    expect(result).toBe('b y');
});