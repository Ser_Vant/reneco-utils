const getResult = require('../getResult');

test('check {w: 3, a: 456, z: \'asdasd\'}', () => {
    const mock = {w: 3, a: 456, z: 'asdasd'},
        result = getResult(mock, 'qweqwe');
    expect(result).toEqual({...mock});
});

test('check (prev) => ({w: 3, a: 456, z: \'asdasd\'})', () => {
    const mock = {w: 3, a: 456, z: 'asdasd'},
        mock2 = 'qweqwe',
        result = getResult((prev) => ({...mock, prev}), mock2);
    expect(result).toEqual({...mock, prev: mock2});
});

test('check (prev) => ({w: 3, a: 456, z: \'asdasd\'})', () => {
    const mock = {w: 3, a: 456, z: 'asdasd'},
        mock2 = 'qweqwe',
        result = getResult((prev) => ({...mock, prev}), mock2);
    expect(result).toEqual({...mock, prev: mock2});
});


test('check (prev, other) => ({w: 3, a: 456, z: \'asdasd\'})', () => {
    const mock = {w: 3, a: 456, z: 'asdasd'},
        mock2 = 'qweqwe',
        mock3 = 'data3',
        result = getResult((prev, other) => ({...mock, prev, other}), mock2, mock3);
    expect(result).toEqual({...mock, prev: mock2, other: mock3});
});

test('check (prev, other) => ({w: 3, a: 456, z: \'asdasd\'}) with functions', () => {
    const mock = {w: 3, a: 456, z: 'asdasd'},
        mock2 = 'qweqwe',
        mock3 = 'data3',
        result = getResult((prev, other) => ({...mock, prev, other}), () => mock2, () => mock3);
    expect(result).toEqual({...mock, prev: mock2, other: mock3});
});