const float = require('../../Format/float');

test('float', () => {
    const mock1 = 5.96E-8;
        result = float(mock1);
    expect(result).toBe('0.0000000596');
});