const parseDate = require('../../Format/parseDate');

test('parseDate dateobject', () => {
    const mock1 = new Date('2014-07-09T07:21:40.51');
        result = parseDate(mock1);
    expect(result).toEqual({
        day: '09',
        hours: '07',
        milliseconds: '510',
        minutes: '21',
        month: '07',
        seconds: '40',
        year: '2014'
    });
});

test('parseDate timestring', () => {
    const mock1 = '2014-07-09T07:21:40.510';
        result = parseDate(mock1);
    expect(result).toEqual({
        day: '09',
        hours: '07',
        milliseconds: '510',
        minutes: '21',
        month: '07',
        seconds: '40',
        year: '2014'
    });
});