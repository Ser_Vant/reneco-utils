const price = require('../../Format/price');

test('price 5.96E-8', () => {
    const mock1 = 5.96E-8;
        result = price({price: mock1});
    expect(result).toBe('0.0 000 000 596');
});
