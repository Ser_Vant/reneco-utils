const datetime = require('../../Format/datetime');

test('datetime instance Date', () => {
    const mock1 = new Date('2014-07-09T07:21:40.51');
        result = datetime({unformatted: mock1});
    expect(result).toBe('2014-07-09 07:21:40.510');
});

test('datetime instance Date str', () => {
    const mock1 = '2014-07-09T07:21:40.510';
        result = datetime({unformatted: mock1});
    expect(result).toBe('2014-07-09 07:21:40.510');
});

test('datetime instance Date str', () => {
    const result = datetime({
        year: '2014',
        month: '07',
        day: '09',
        hours: '07',
        minutes: '21',
        seconds: '40',
        milliseconds: '510'
    });
    expect(result).toBe('2014-07-09 07:21:40.510');
});