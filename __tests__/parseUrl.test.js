const parseUrl = require('../parseUrl');

test('http://domen2.domen1/some/path?q1=1&q2=two#anchor', () => {
    const mock = 'http://domen2.domen1/some/path?q1=1&q2=two#anchor',
        result = parseUrl(mock);
    expect(result).toEqual({
        hash: 'anchor',
        host: 'domen2.domen1',
        pathname: '/some/path',
        protocol: 'http',
        query: {
            q1: '1', q2: 'two'
        }
    });
});

test('http://domen2.domen1', () => {
    const mock = 'http://domen2.domen1',
        result = parseUrl(mock);
    expect(result).toEqual({
        hash: undefined,
        host: 'domen2.domen1',
        pathname: undefined,
        protocol: 'http',
        query: undefined
    });
});