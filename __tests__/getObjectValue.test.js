const getObjectValue = require('../getObjectValue'),
    obj = {
        a: 1,
        b: 2,
        c: {
            d: 3,
            e: {
                g: 5,
                h: 8
            },
            f: 4
        }
    };

test('empty', () => {
    expect(getObjectValue(null, null)).toBeUndefined();
    expect(getObjectValue(null, undefined)).toBeUndefined();
    expect(getObjectValue(obj, null)).toBeUndefined();
    expect(getObjectValue(obj, undefined)).toBeUndefined();
});

test('get simple key', () => {
    const mock = 'a',
    result = getObjectValue(obj, mock);
    expect(result).toBe(1);
});


test('get with one delimiter', () => {
    const mock = 'c.d',
    result = getObjectValue(obj, mock);
    expect(result).toBe(3);
});

test('get with two delimiters', () => {
    const mock = 'c.e.g',
    result = getObjectValue(obj, mock);
    expect(result).toBe(5);
});

test('get with two delimiters and delimiter', () => {
    const mock = 'c**e**g',
    result = getObjectValue(obj, mock, '**');
    expect(result).toBe(5);
});