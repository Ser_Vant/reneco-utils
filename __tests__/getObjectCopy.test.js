const getObjectCopy = require('../getObjectCopy');

test('null', () => {
    const mock = null,
        result = getObjectCopy(mock);
    expect(result).toEqual(null);
});


test('245', () => {
    const mock = 245,
        result = getObjectCopy(mock);
    expect(result).toEqual(mock);
});

test('{}', () => {
    const mock = {},
        result = getObjectCopy(mock);
    expect(result).toEqual(mock);
});

test('[1,2,3]', () => {
    const mock = [1,2,3],
        result = getObjectCopy(mock);
    mock[0] = 2334;
    expect(result).toEqual([1,2,3]);
});

test('{q: 1, w: 34}', () => {
    const mock = {q: 1, w: 34},
        result = getObjectCopy(mock);
    mock.q = 900;
    expect(result).toEqual({q: 1, w: 34});
});

test('{q: 1, w: 34, ff: [{x: 1, xx: 2}]}', () => {
    const mock = {q: 1, w: 34, ff: [{x: 1, xx: 2}]},
        result = getObjectCopy(mock);
    mock.q = 900;
    mock.ff[0].y = 'sss';
    expect(result).toEqual({q: 1, w: 34, ff: [{x: 1, xx: 2}]});
});