const isString = require('../isString'), 
    falseCases = [
        [ 'null', null ],
        [ 'undefined', undefined ],
        [ 'object', { a: 1, b: 2 } ],
        [ 'array', [ 1, 2, 3 ] ],
        [ 'number', 123 ],
        [ 'zero', 0 ],
        [ 'RegExp', new RegExp('hello') ],
        [ 'number with valueOf returning string', Object.assign(10, { valueOf: () => 'abc' }) ],
        [ 'object pretending to be string', { constructor: String } ]
    ],
    trueCases = [
        [ 'empty literal string', '' ],
        [ 'unicode string literal', String.fromCharCode(10000) ],
        [ 'empty boxed string', new String('') ],
        [ 'unicode boxed string', new String(String.fromCharCode(10000)) ],
        [ 'string with overwritten "constructor"', Object.assign('hi', { constructor: Array }) ],
        [ 'string with overwritten "toString"', Object.assign('hi', { toString: 123 }) ],
        [ 'string with overwritten "valueOf"', Object.assign('hi', { valueOf: 123 }) ],
        [ 'string with overwritten "constructor"', Object.assign('hi', { constructor: RegExp }) ],
        [ 'proxied string', new Proxy(new String('hello'), {}) ]
    ];

falseCases.forEach(([title, value]) => test(title, () => {
    expect(isString(value)).toBeFalsy();
}));

trueCases.forEach(([title, value]) => test(title, () => {
    expect(isString(value)).toBeTruthy();
}));