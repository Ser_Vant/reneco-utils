const fromCSS = require('../fromCSS');

test('border-bottom-color', () => {
    const mock = 'border-bottom-color',
        result = fromCSS(mock);
    expect(result).toBe('borderBottomColor');
});