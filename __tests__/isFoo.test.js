const isFoo = require('../isFoo');

test('function() {}', () => {
    const mock = function() {},
        result = isFoo(mock);
    expect(result).toBeTruthy();
});

test('class {}', () => {
    class mock {};
    const result = isFoo(mock);
    expect(result).toBeTruthy();
});

test('null', () => {
    const mock = null,
        result = isFoo(mock);
    expect(result).toBeFalsy();
});

test('1', () => {
    const mock = 1,
        result = isFoo(mock);
    expect(result).toBeFalsy();
});

test('new Date()', () => {
    const mock = new Date(),
        result = isFoo(mock);
    expect(result).toBeFalsy();
});

test('{}', () => {
    const mock = {},
        result = isFoo(mock);
    expect(result).toBeFalsy();
});