const getClassesObject = require('../getClassesObject');

test('a z b', () => {
    const mock = 'a z b',
        result = getClassesObject(mock);
    expect(result).toEqual({a: true, z: true, b: true});
});