const parseGetString = require('../parseGetString');

test('?a=1&b=123&z=qwe', () => {
    const mock1 = '?a=1&b=123&z=qwe',
        result = parseGetString(mock1);
    expect(result).toEqual({
        a: '1',
        b: '123',
        z: 'qwe'
    });
});

test('?id=0&params[confirm]=a&params[email]=b&params[password]=c', () => {
    const mock1 = '?id=0&params[confirm]=a&params[email]=b&params[password]=c',
        result = parseGetString(mock1);
    expect(result).toEqual({
        id: '0',
        params: {
            confirm: 'a',
            email: 'b',
            password: 'c'
        }
    });
});

test('?id=0&params[a][confirm]=a&params[a][email]=b&params[password]=c', () => {
    const mock1 = '?id=0&params[a][confirm]=a&params[a][email]=b&params[password]=c',
        result = parseGetString(mock1);
    expect(result).toEqual({
        id: '0',
        params: {
            a: {
                confirm: 'a',
                email: 'b',
            },
            password: 'c'
        }
    });
});

test('empty string', () => {
    const mock1 = '',
        result = parseGetString(mock1);
    expect(result).toEqual({});
});

test('?local=абвгд', () => {
    const mock1 = '?local=абвгд',
        result = parseGetString(mock1);
    expect(result).toEqual({
        local: 'абвгд'
    });
});