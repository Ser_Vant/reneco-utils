const getNameSpace = require('../getNameSpace');

test('some.name.space', () => {
    const mock = 'some.name.space',
        result = getNameSpace(mock);
    expect(window.some.name.space).toBeDefined();
});