const toCSS = require('../toCSS');

test('borderBottomColor', () => {
    const mock = 'borderBottomColor',
        result = toCSS(mock);
    expect(result).toBe('border-bottom-color');
});