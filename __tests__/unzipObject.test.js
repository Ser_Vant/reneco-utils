const unzipObject = require('../unzipObject');

test('{foo: "bar", fooz: [1, 2, 3], a:1, b: [23, 234, 34]}', () => {
    const mock = {foo: 'bar', fooz: [1, 2, 3], a:1, b: [23, 234, 34]},
        result = unzipObject(mock);
    expect(result).toBe('foo=bar&fooz=1,2,3&a=1&b=23,234,34');
});

test('{id: 0, params: {confirm: "a", email: "b", password: "c"}}', () => {
    const mock = {
            id: 0,
            params: {
                confirm: 'a',
                email: 'b',
                password: 'c'
            }
        },
        result = unzipObject(mock);
    expect(result).toBe(encodeURI('id=0&params[confirm]=a&params[email]=b&params[password]=c'));
})

test('undefined', () => {
    const mock = undefined,
        result = unzipObject(mock);
    expect(result).toBe('');
});

test('', () => {
    const mock = {
        id: 0,
        params: {
            pp: {a: 'a', b: 'b'},
            ppp: [1,2,3]
        },
        X: null,
        ooo: undefined
    }, result = unzipObject(mock);
    expect(result).toBe(encodeURI('id=0&params[pp][a]=a&params[pp][b]=b&params[ppp]=1,2,3'))
})