const isArrayEqual = require('../isArrayEqual');

test("['foo', 'bar'], []", () => {
    const mock1 = ['foo', 'bar'],
        mock2 = [],
        result = isArrayEqual(mock1, mock2);
    expect(result).toBeFalsy();
});

test("[], ['foo', 'bar']", () => {
    const mock2 = ['foo', 'bar'],
        mock1 = [],
        result = isArrayEqual(mock1, mock2);
    expect(result).toBeFalsy();
});

test("[], []", () => {
    const mock2 = [],
        mock1 = [],
        result = isArrayEqual(mock1, mock2);
    expect(result).toBeTruthy();
});

test("[1], [1]", () => {
    const mock2 = [1],
        mock1 = [1],
        result = isArrayEqual(mock1, mock2);
    expect(result).toBeTruthy();
});

test("[1], undefined", () => {
    const mock1 = [1],
        mock2 = undefined,
        result = isArrayEqual(mock1, mock2);
    expect(result).toBeFalsy();
});

test("{}, undefined", () => {
    const mock1 = {},
        mock2 = undefined,
        result = isArrayEqual(mock1, mock2);
    expect(result).toBeFalsy();
});

test("[1,2,3,4,5,6,7,8,9], [1,2,3,4,5,6,7,8,9]", () => {
    const mock1 = [1,2,3,4,5,6,7,8,9],
        mock2 = [1,2,3,4,5,6,7,8,9],
        result = isArrayEqual(mock1, mock2);
    expect(result).toBeTruthy();
});

test("[1,2,3,4,5,6,7,8,2], [1,2,3,4,5,6,7,8,9]", () => {
    const mock1 = [1,2,3,4,5,6,7,8,2],
        mock2 = [1,2,3,4,5,6,7,8,9],
        result = isArrayEqual(mock1, mock2);
    expect(result).toBeFalsy();
});