const createUrl = require('../createUrl');

test("['foo', 'bar'], {a: 1, b: 'two'}, anchor", () => {
    const mock1 = ['foo', 'bar'],
        mock2 = {a: 1, b: 'two'},
        mock3 = 'anchor'
        result = createUrl(mock1, mock2, mock3);
    expect(result).toBe('foo/bar?a=1&b=two#anchor');
});