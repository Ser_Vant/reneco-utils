const getHash = require('../getHash');

test('check {w: 3, a: 456, z: "asdasd"}', () => {
    const mock = {w: 3, a: 456, z: 'asdasd'},
        result = getHash(mock);
    expect(result).toBe('a=456&w=3&z=asdasd');
});