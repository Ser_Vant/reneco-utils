const defaults = require('../defaults');

test('{a: 12,b: \'x\', c: null, d: 100}, { a: 600, c: undefined, d: 0}', () => {
    const mock1 = {
            a: 12,
            b: 'x',
            c: null,
            d: 100
        },
        mock2 = {
            a: 600,
            c: undefined,
            d: 0,
            x: 100
        },
        result = defaults(mock1, mock2);
    expect(result).toEqual({
        a: 600,
        b: 'x',
        c: undefined,
        d: 0,
        x: 100
    });
});