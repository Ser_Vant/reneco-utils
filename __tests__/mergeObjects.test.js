const mergeObjects = require('../mergeObjects');

test('simple merge', () => {
    const obj1 = {a: 1},
        obj2 = {b: 2};
    expect(mergeObjects(obj1, obj2)).toEqual({
        a: 1,
        b: 2
    });
});

test('with array', () => {
    const obj1 = {a: [1,2,3]},
        obj2 = {a: [4,5,6]};
    expect(mergeObjects(obj1, obj2)).toEqual({
        a: [1,2,3,4,5,6]
    });
});

test('with object', () => {
    const obj1 = {a: {q: 1, w: 2, e: 3}},
        obj2 = {a: {a: 4, s: 5, d: 6}};
    expect(mergeObjects(obj1, obj2)).toEqual({
        a: {
            q:1,w:2,e:3,
            a:4,s:5,d:6
        }
    });
});