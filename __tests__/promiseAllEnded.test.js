const promiseAllEnded = require('../promiseAllEnded');

test('promiseAllEnded', () => {
    expect.assertions(1);
    const e = new Error('EEEE');
    return promiseAllEnded([
            Promise.reject(e),
            Promise.resolve('Q'),
            Promise.resolve('true'),
            Promise.resolve('122'),
            Promise.reject('RRRRRR'),
            Promise.resolve(true),
            Promise.resolve(4345),
        ]).then(result => expect(result).toEqual([
            e,
            'Q',
            'true',
            '122',
            'RRRRRR',
            true,
            4345
        ]));
});