const getClassesObject = require('./getClassesObject'),
    getClasses = require('./getClasses');

module.exports = function addClass() {
    return getClasses(Object.values(arguments).reduce((list, cl) => Object.assign(list, getClassesObject(cl)), {}));
}