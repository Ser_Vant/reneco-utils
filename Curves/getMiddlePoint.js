'use strict';

const getMiddlePoints = require('./getMiddlePoints');

module.exports = curve => (firstMidpoints => (secondMidpoints => getMiddlePoints(secondMidpoints)[0])(getMiddlePoints(firstMidpoints)))(getMiddlePoints(curve));