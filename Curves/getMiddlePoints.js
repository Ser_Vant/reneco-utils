'use strict';

module.exports = pointList => pointList.slice(0, -1).map((p, ix) => [
    (pointList[ix][0] + pointList[ix + 1][0])/2.0,
    (pointList[ix][1] + pointList[ix + 1][1])/2.0
])