'use strict';

module.exports = function extend(obj, newMethods) {
    let dafaultPrefix = '__';
    Object.keys(newMethods).forEach(title=>{
        let body = newMethods[title];
        if (obj.prototype[title]) {
            var prefix = dafaultPrefix;
            while (obj.prototype[prefix + title]) {
                prefix += prefix;
            }
            obj.prototype[prefix + title] = obj.prototype[title];
        }
        obj.prototype[title] = body;
    });
    return obj;
}
