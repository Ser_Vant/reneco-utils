'use strict';
module.exports = function isFoo (foo) {
    return foo && {}.toString.call(foo) === '[object Function]';
};
