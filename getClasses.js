'use strict';
module.exports = function getClasses(o) {
    return Object.keys(o || {})
        .map(key => o[key] && key || null)
        .reduce((prev, cur) => {
            cur && prev.push(cur);
            return prev;
        }, [])
        .join(' ');
};
