'use strict';
const ajaxPromissed = require('./ajaxPromised');

let scriptPromises = {};

module.exports = function loadScript (scriptUrl) {
    if (!scriptPromises[scriptUrl]) {
        scriptPromises[scriptUrl] = ajaxPromissed(scriptUrl, {
            method: 'get',
            dataType: 'script',
            // success: resolve,
            // error: reject
        });
    }
    return scriptPromises[scriptUrl];
}
