const noop = require('./noop'),
    defaults = require('./defaults'),
    unzipObject = require('./unzipObject');



module.exports = function ajax(url, dataObject) {
    const _JSON = 'json',
        _JSONTYPE = 'application/json';
    let object = defaults({
        isBinary: false,
        url: '',
        method: 'GET',
        data: {},
        file: null,
        dataType: 'json',
        // contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: {},
        load: noop,
        success: noop,
        error: noop,
        progress: noop,
        abort: noop,
        sync: false,
        messager: null
    }, Object.assign({}, dataObject || url));
    object.url = dataObject ? url : object.url;
    let method = ((object.file || object.isBinary) ? 'POST' : (object.type || object.method)).toUpperCase(),
        data = (object.isBinary && object.data) || object.file || (unzipObject(object.data)),
        request = new XMLHttpRequest(),
        error;
    object.headers['X-Requested-With'] = 'XMLHttpRequest';
    if (object.file) {
        object.headers['X-File-Name'] = encodeURIComponent(object.file.name);
        object.headers['X-File-Size'] = object.file.size;
        object.headers['X-File-Type'] = object.file.type;

    } else if(object.contentType){
        object.headers['Content-Type'] = object.contentType;
    }
    console.log(request);
    request.open(method, object.url + (method === 'GET' ? ((object.data && '?') + data) : ''), !object.sync);
    Object.keys(object.headers)
        .forEach(name => request.setRequestHeader(name, object.headers[name]));
    request.upload.onprogress = object.progress;

    request.onload = () => {
        object.load();
        // console.log('status', request.status)
        if(request.status >= 200 && request.status < 400) {
            try {
                object.success((object.dataType === _JSON || object.dataType === _JSONTYPE)
                    ? (request.responseText ? JSON.parse(request.responseText) : {})
                    : request.responseText);      
                if (object.messager && object.messager.info) {
                    object.messager.info('success');
                }                                 
            } catch (e) {
                error = e;
            }
        } else {
            try {
                error = {
                    status: request.status,
                    message: request.statusText,
                    data: (answer => answer.data || answer)(object.dataType === _JSON ? JSON.parse(request.responseText) : request.responseText)
                };  
            } catch (e) {
                error = {
                    status: request.status,
                    message: request.responseText
                };
            }
        }
        if (error) {
            if (object.messager && object.messager.error) {
                object.messager.error(error.message || error);
            }
            object.error(error);
        }
    };

    request.onabort = object.abort;

    request.onerror = () => {
        object.error({
            status: request.status,
            message: request.statusText
        });
    };
    request.send((method === 'POST' ? data : ''));
    return request;
}
