'use strict';
module.exports = function getFileSlice(file, start, end) {
    let fName = ['slice', 'webkitSlice', 'mozSlice'].find(f => {
        return file[f] !== undefined;
    });
    return fName && file[fName](start, end);
}
