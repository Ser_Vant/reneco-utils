'use strict';
let counter = 0;
module.exports = function attachScript({url, attributes, id, node}) {
    return ((d, s, id) => !d.getElementById(id)
        ? new Promise((resolve, reject) => {
                const fjs = d.getElementsByTagName(s)[0],
                    sc = document.createElement(s);
                (attrs => Object.keys(attrs).forEach(attr => sc.setAttribute(attr, attrs[attr])))(Object.assign({src: url, id}, attributes));
                sc.onload = resolve;
                sc.onerror = reject;
                sc.onabort = reject;
                if(node) {
                    node.appendChild(sc);
                } else {
                    fjs.parentNode.insertBefore(sc, fjs);
                }
            })
        : Promise.resolve()
    )(document, 'script', id || `attachScript-${counter++}`);
}