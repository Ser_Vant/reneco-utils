'use strict';
const getDocumentOffset = require('./getDocumentOffset');

module.exports = function getCurrentWidth() {
    let screenWidth = getDocumentOffset().width;
    if (screenWidth >= 1200) {
        return 'lg';
    } else if (screenWidth >= 980) {
        return 'md';
    } else if (screenWidth >= 768) {
        return 'sm';
    } else {
        return 'xs';
    }
}
