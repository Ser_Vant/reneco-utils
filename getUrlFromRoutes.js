'use strict';
module.exports = function getUrlFromRoutes (routes, params, basePath) {
    let getRouteName = routePath => {
            let ret = routePath;
            if (ret.indexOf(':') > -1) {
                ret = ret.replace(/:([^:/]+)/g, (finded, paramName) => {
                    return params[paramName] || '';
                });
            }
            return ret;
        },
        allRoutes = routes.map(route => {
            return route.path;
        });
    if (basePath) {
        let ix = basePath && allRoutes.findIndex(route => {
            return route === basePath;
        });
        allRoutes = ix > -1 ? allRoutes.slice(0, ix + 1) : [];
    }
    return (allRoutes.map(getRouteName)).join('/');
}
