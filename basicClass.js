'use strict';

const getExtended = require('./getExtended'),
    baseClass = function (data) { this.init(data); };
baseClass.prototype = { init() { } };

module.exports = function basicClass (newMethods) {
    return getExtended(baseClass, newMethods);
}
