'use strict';

module.exports = function fromPairs(pairs, o) {
    return Array.isArray(pairs)
        ? pairs.reduce((list, item) => {
            list[item[0]] = item[1];
            return list;
        }, o || {})
        : {};
}