'use strict';

module.exports = function createUrl (paths, query, hash) {
    const path = (Array.isArray(paths) ? paths : [paths]).join('/'),
        plainQuery = Object.keys(query || {}).map(key => `${key}=${query[key]}`).join('&');
    return path + (plainQuery ? ('?' + plainQuery) : '') + (hash ? ('#' + hash) : '');
}
