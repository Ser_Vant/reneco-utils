'use strict';

const RE = new RegExp('^(\\d{4})(-(\\d{2})(-(\\d{2}))?)?$');

module.exports = function parseDate (dateSting) {
    if (dateSting) {
        let r = RE.exec(dateSting),
            month = parseInt((r[3] || '').replace(/^0/, '')) - 1,
            day = (r[5] || '').replace(/^0/, '');
        return {
            year: dateSting && parseInt(r[1]),
            month: dateSting && (month > -1 ? month : null),
            day: dateSting && day && parseInt(day) || null
        };
    } else {
        return {
            year: null,
            month: null,
            day: null
        };
    }
}
