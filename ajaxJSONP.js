'use strict';

const attachScript = require('./attachScript'),
    unzipObject = require('./unzipObject');

let counter = 0;

module.exports = function ajaxJSONP(url, params) {
    return new Promise(resolve => ((scid) => {
        window[scid] = resolve;
        attachScript({
            url:`${url}${url.indexOf('?') > -1 ? '&' : '?'}${unzipObject(Object.assign({
                callback: scid
            }, params))}`,
            id: scid,
            attributes: {
                async: true,
            }
        })
            .then(() => {
                const sc = document.getElementById(scid);
                sc.parentNode.removeChild(sc);
            });
    }
    )(`ajaxJSONP${counter++}`));
}