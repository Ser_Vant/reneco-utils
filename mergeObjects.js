'use strict';

const isObject = require('./isObject');

module.exports = function mergeObjects(a, b) {
    return Object.keys(a)
        .concat(Object.keys(b))
        .reduce((merged, k) => {
            if(merged[k] === undefined) {
                if(a[k] !== undefined) {
                    merged[k] = a[k];
                }
                if(b[k] !== undefined) {
                    if(a[k] === undefined) {
                        merged[k] = b[k];
                    } else {
                        if(Array.isArray(merged[k])) {
                            merged[k] = merged[k].concat(b[k]);
                        } else if(isObject(merged[k])) {
                            merged[k] = Object.assign(merged[k], b[k]);
                        }
                    }
                }
            }
            return merged;
        }, {});
};
