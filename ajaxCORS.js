'use strict';

const PromiseRejectable = require('./PromiseRejectable'),
    unzipObject = require('./unzipObject');

module.exports = function ajaxCORS(url, {
    dataType = 'json',
    method = 'post',
    headers = [],
    data
}) {
    return ((method, data) => {
        return new PromiseRejectable((resolve, reject) => {
            const xhr = (xxhhrr => 'withCredentials' in xxhhrr
                ? xxhhrr
                : (XDomainRequest ? new XDomainRequest : null)
            )(new XMLHttpRequest());
            if (xhr) {
                Object.keys(headers || {}).forEach(header => xhr.setRequestHeader(header, headers[header]));
                xhr.open(method, url + (method === 'GET' ? ((data && (url.indexOf('?') >-1 ? '&': '?')) + data) : ''), true);
                xhr.onload = data => {
                    let answer, error;
                    try {
                        switch (dataType) {
                        case 'json':
                            answer = JSON.parse(xhr.responseText);
                            break;
                        default:
                            answer = xhr.responseText;
                        }
                        if (xhr.status >= 200 && xhr.status < 400) {
                            resolve(answer);
                        } else {
                            error = {
                                status: xhr.status,
                                message: xhr.statusText,
                                data: answer.data || answer
                            };
                        }
                    } catch (e) {
                        error = e;
                    }
                    if (error) {
                        reject(error);
                    }
                };
                xhr.onerror = reject;
                xhr.onabort = reject;
                xhr.send((method === 'POST' ? data : ''));
            }
        })
    })(method.toUpperCase(), unzipObject(data));
}