'use strict';
module.exports = function getNameSpace (namespace) {
    if (!Array.isArray(namespace)) { namespace = namespace.split('.'); }
    let curNS = window;
    namespace.forEach(namespacePart => {
        if (!curNS[namespacePart]) { curNS[namespacePart] = {}; }
        curNS = curNS[namespacePart];
    });
    return curNS;
}
