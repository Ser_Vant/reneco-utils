'use strict';

const ajax = require('./ajax'),
    noop = require('./noop');

module.exports = function ajaxBinary(url, dataObject){
    const object = Object.assign({}, {
            url: '',
            dataType: 'json',
            data: '',
            file: null,
            headers: {},
            success: noop,
            error: noop,
            progress: noop
        }, (dataObject || url)),
        ords = Array.prototype.map.call(object.data||'', x => x.charCodeAt(0) & 0xff),
        ui8a = new Uint8Array(ords);
    url = dataObject ? url : object.url;
    object.data = ui8a.buffer;
    object.contentType = 'application/x-binary; charset=x-user-defined';
    object.isBinary = true;
    return ajax(url, object);
}
