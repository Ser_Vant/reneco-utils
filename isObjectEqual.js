'use strict';

const isArrayEqual = require('./isArrayEqual'),
    isObject = require('./isObject'),
    prepareValue = v => isObject(v) ? v : (v?{v}:{});

module.exports = function isObjectEqual(o1, o2) {
    o1 = prepareValue(o1);
    o2 = prepareValue(o2);
    const compareArray = (a, b) => Array.isArray(a)
        ? isArrayEqual(a, b, compareArray)
        : (isObject(a)
            ? isObjectEqual(a, b)
            : a == b
        );

    return ((ks1, ks2) => {
        return isArrayEqual(ks1, ks2) && ks1.findIndex(k1 => {
            return Array.isArray(o1[k1])
                ? !isArrayEqual(o1[k1], o2[k1], compareArray)
                : (isObject(o1[k1])
                    ? !isObjectEqual(o1[k1], o2[k1])
                    : o1[k1] != o2[k1]
                )
        }) < 0
    })(Object.keys(o1), Object.keys(o2));
}