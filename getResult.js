'use strict';
const isFoo = require('./isFoo');

module.exports = function getResult() {
    const callback = arguments[0];
    // console.log(arguments)
    return isFoo(callback) 
        ? callback.apply(null, Array.prototype.slice.call(arguments, 1).map(arg => isFoo(arg) ? arg() : arg)) 
        : callback;
}