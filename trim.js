'use strict';

module.exports = function trim (v) {
    return v.replace ? v.replace(/^\s+/, '').replace(/\s+$/, '') : v;
};
