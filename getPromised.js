'use strict';
module.exports = function getPromised(data) {
    return !(data instanceof Promise)
        ? (data
            ? Promise.resolve(data)
            : Promise.reject(data)
        )
        : data;
};
